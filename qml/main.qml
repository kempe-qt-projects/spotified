import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtWebEngine 1.3

ApplicationWindow {
    visible: true
    width: 480
    height: 680
    title: qsTr("Spotified")

    property alias stack: stack

    StackView {
        id: stack
        anchors.fill: parent
        initialItem: "qrc:/pages/Main/MainPage.qml";

        replaceEnter: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 0
                to:1
                duration: 200
            }
        }
        replaceExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to:0
                duration: 200
            }
        }
    }
}
