import QtQuick 2.7
import QtQuick.Controls 2.2

Page {
    property alias authenticateButton: authenticateButton
    property int replace_animation: StackView.ReplaceTransition

    BusyIndicator {
        id: spinner
        anchors.centerIn: parent
        running: Spotified.authenticationRunning
    }

    Pane {
        anchors.fill: parent
        visible: !spinner.running && !Spotified.authenticated

        Button {
            id: authenticateButton
            text: qsTr("Authorize Spotified")
            anchors.centerIn: parent
        }
    }
}
