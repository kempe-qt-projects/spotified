import QtQuick 2.6
import Sailfish.Silica 1.0

Page {
    property alias authenticateButton: authenticateButton
    property int replace_animation

    BusyIndicator {
        id: spinner
        anchors.centerIn: parent
        running: Spotified.authenticationRunning
    }

    SilicaFlickable {
        anchors.fill: parent
        visible: !spinner.running && !Spotified.authenticated

        Button {
            id: authenticateButton
            text: qsTr("Authorize Spotified")
            anchors.centerIn: parent
        }
    }
}
