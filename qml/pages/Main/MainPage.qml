import QtQuick 2.6

MainPageForm {

    Connections {
        target: authenticateButton
        onClicked: stack.push("qrc:/pages/SpotifyAuthentication/SpotifyAuthenticatePage.qml")
    }

    Connections {
        target: Spotified
        onAuthenticatedChanged: {
            if (Spotified.authenticated) {
                stack.replace("qrc:/pages/HomeProfile/HomeProfilePage.qml", replace_animation)
            }
        }
    }
}
