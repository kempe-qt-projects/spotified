import QtQuick 2.6

HomeProfilePageForm {
    Component.onCompleted: {
        homeProfile.init()
        homePlaylists.init()
    }
}
