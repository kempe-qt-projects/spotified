import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import it.kempe.Spotified 1.0

Page {
    property alias homeProfile: homeProfile
    property alias homePlaylists: homePlaylists
    padding: 10

    SpotifyHomeProfile {
        id: homeProfile
        spotifyClient: SpotifyClient
    }
    SpotifyHomePlaylists {
        id: homePlaylists
        spotifyClient: SpotifyClient
    }

    header: ToolBar {
        RowLayout {
            anchors.fill: parent
            Item {
                width: menuButton.width
            }

            Label {
                text: homeProfile.displayName
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }

            ToolButton {
                id: menuButton
                background: Image {
                    source: homeProfile.profileImage
                    smooth: true
                    fillMode: Image.PreserveAspectFit
                    sourceSize.height: height
                    sourceSize.width: height
                }
            }
        }
    }

    ListView {
        id: listView
        anchors.fill: parent

        displayMarginBeginning: 40
        displayMarginEnd: 40
        spacing: 5

        delegate: ItemDelegate {
            RowLayout {
                anchors.fill: parent
                Image {
                    source: model.image
                    height: parent.height
                    sourceSize.height: height
                }
                Label {
                    text: model.name
                }
            }
        }
        model: homePlaylists
    }
}
