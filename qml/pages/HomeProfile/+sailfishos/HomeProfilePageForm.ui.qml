import QtQuick 2.6
import Sailfish.Silica 1.0
import it.kempe.Spotified 1.0

Page {
    property alias homeProfile: homeProfile
    property alias homePlaylists: homePlaylists

    SpotifyHomeProfile {
        id: homeProfile
        spotifyClient: SpotifyClient
    }
    SpotifyHomePlaylists {
        id: homePlaylists
        spotifyClient: SpotifyClient
    }

    ListView {
        id: listView

        anchors.fill: parent
        anchors.leftMargin: Theme.horizontalPageMargin
        anchors.rightMargin: Theme.horizontalPageMargin

        displayMarginBeginning: 40
        displayMarginEnd: 40
        spacing: Theme.paddingMedium

        header: PageHeader {
            id: header

            Image {
                id: profileImage
                parent: header.extraContent

                height: Theme.itemSizeSmall
                width: Theme.itemSizeSmall
                sourceSize.height: height
                sourceSize.width: width
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right

                source: homeProfile.profileImage
                fillMode: Image.PreserveAspectFit
            }

            Label {
                parent: header.extraContent

                text: homeProfile.displayName
                width: Math.min(implicitWidth, parent.width - leftMargin - profileImage.width - rightMargin - rightMar)
                truncationMode: TruncationMode.Fade
                color: Theme.highlightColor
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter

                anchors.right: profileImage.left
                anchors.rightMargin: header.rightMargin
                anchors.verticalCenter: parent.verticalCenter

                font.pixelSize: Theme.fontSizeLarge
                font.family: Theme.fontFamilyHeading
            }
        }

        delegate: ListItem {
            Row {
                anchors.fill: parent
                spacing: Theme.paddingMedium

                Image {
                    source: model.image
                    height: parent.height
                    sourceSize.height: height
                }

                Label {
                    text: model.name
                }
            }
        }

        model: homePlaylists
    }
}
