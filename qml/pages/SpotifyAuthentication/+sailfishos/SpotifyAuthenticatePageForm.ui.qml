import QtQuick 2.6
import Sailfish.Silica 1.0

Page {
    property alias url: webview.url
	
	SilicaWebView {
		id: webview
		anchors.fill: parent
	}
}
