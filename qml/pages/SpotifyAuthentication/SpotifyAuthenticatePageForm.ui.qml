import QtQuick 2.6
import QtQuick.Controls 2.0
import QtWebEngine 1.3

Page {
    property alias url: webview.url
	
	WebEngineView {
		id: webview
		anchors.fill: parent
	}
}
