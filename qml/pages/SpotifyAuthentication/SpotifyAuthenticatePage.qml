import QtQuick 2.6

SpotifyAuthenticatePageForm {
    property string baseUrl: "https://accounts.spotify.com/authorize"
    property string clientId: "?client_id=" + Spotified.appId
    property string scopes: "&scope=playlist-read-private user-read-private playlist-read-collaborative user-library-read user-top-read user-read-playback-state user-read-currently-playing user-read-recently-played"
    property string responseType: "&response_type=code"
    property string redirect: "&redirect_uri=" + Spotified.callbackUrl

    url: baseUrl + clientId + scopes + responseType + redirect

    Connections {
        target: Spotified
        onAuthenticatedChanged: Spotified.authenticated ? stack.replace("qrc:/Pages/HomeProfilePage.qml") : stack.pop();
    }

    Component.onCompleted: {
        Spotified.requestAccess()
    }
}
