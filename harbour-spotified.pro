
TARGET = harbour-spotified

CONFIG += sailfishapp

HEADERS += $$files(src/*.h)

SOURCES += \
    src/harbour-spotified.cpp \
    src/spotified.cpp \
    src/spotify_authentication_server.cpp \
    src/spotifyhomeplaylists.cpp \
    src/spotifyhomeprofile.cpp \
    src/spotifywebclient.cpp

DISTFILES += \
    rpm/harbour-spotified.changes.in \
    rpm/harbour-spotified.changes.run.in \
    rpm/harbour-spotified.spec \
    rpm/harbour-spotified.yaml \
    translations/*.ts \
    harbour-spotified.desktop

RESOURCES += qml/qml.qrc

QML_IMPORT_PATH = $$PWD/libs

SAILFISHAPP_ICONS = 86x86 108x108 128x128

CONFIG += sailfishapp_i18n

TRANSLATIONS += translations/harbour-spotified-de.ts
