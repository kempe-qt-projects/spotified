#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtWebEngine>
#include "spotified.h"
#include "spotifyhomeprofile.h"
#include "spotifyhomeplaylists.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setOrganizationName("Kempe");
    QCoreApplication::setOrganizationDomain("kempe.it");
    QCoreApplication::setApplicationName("Spotified");

    QGuiApplication app(argc, argv);

    QtWebEngine::initialize();
    QQmlApplicationEngine engine;

    Spotified spotified(qApp);
    engine.rootContext()->setContextProperty("Spotified", &spotified);
    engine.rootContext()->setContextProperty("SpotifyClient", spotified.spotifyClient());

    qmlRegisterType<SpotifyHomeProfile>("it.kempe.Spotified", 1, 0, "SpotifyHomeProfile");
    qmlRegisterType<SpotifyHomePlaylists>("it.kempe.Spotified", 1, 0, "SpotifyHomePlaylists");

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
