#include "spotifywebclient.h"

SpotifyWebClient::SpotifyWebClient(QObject *parent, const QString &clientId, const QString &clientSecret) : QObject(parent) {
    manager = new QNetworkAccessManager(this);
    createAuthenticationToken(clientId, clientSecret);
}

SpotifyWebClient::~SpotifyWebClient() {
    if (manager != NULL)
        manager->deleteLater();
}

void SpotifyWebClient::setTokenInformation(QString accessToken, QString refreshToken, QString tokenType) {

    this->accessToken = accessToken;
    this->refreshToken = refreshToken;
    this->tokenType = tokenType;
}

void SpotifyWebClient::createAuthenticationToken(const QString &clientId, const QString &clientSecret) {
    QString concatenated = clientId + ":" + clientSecret;
    QByteArray tokenData = concatenated.toLocal8Bit().toBase64();
    authenticationToken = QString("Basic " + tokenData).toLocal8Bit();
}

void SpotifyWebClient::refreshAccessToken(QString refreshToken, QString callbackUrl) {
    QUrlQuery params;
    if (accessToken.isEmpty()) {
        params.addQueryItem("grant_type", "authorization_code");
        params.addQueryItem("code", refreshToken);
        params.addQueryItem("redirect_uri", callbackUrl);
    } else {
        params.addQueryItem("grant_type", "refresh_token");
        params.addQueryItem("refresh_token", refreshToken);
    }

    QNetworkRequest request = QNetworkRequest(API_REFRESH_TOKEN);
    request.setHeader(QNetworkRequest::ContentTypeHeader, MIME_TYPE_XFORM);
    request.setRawHeader("Authorization", authenticationToken);

    qDebug() << "Sending refreshAccessToken with: " << params.toString();
    QNetworkReply *reply = manager->post(request, encodeParams(params));
    connect(reply, &QNetworkReply::finished, this, [=]() { handleRefreshAccessTokenCompleted(reply); });
    connect(reply, &QNetworkReply::finished, reply, &QNetworkReply::deleteLater);

}

void SpotifyWebClient::handleRefreshAccessTokenCompleted(QNetworkReply *reply) {
    const auto jsonData = reply->readAll();
    const auto document = QJsonDocument::fromJson(jsonData);
    Q_ASSERT(document.isObject());

    const auto response = document.object().toVariantMap();
    emit accessTokenUpdated(response);
    reply->deleteLater();
}

QNetworkReply *SpotifyWebClient::sendGetRequest(QUrl url) {
    QNetworkRequest request = QNetworkRequest(url);
    const auto authenticationHeader = QString(tokenType + " " + accessToken).toLocal8Bit();
    request.setRawHeader("Authorization", authenticationHeader);

    return manager->get(request);
}


QByteArray SpotifyWebClient::encodeParams(const QUrlQuery &params) {
    return params.query(QUrl::FullyEncoded).toUtf8();
}
