#ifndef SPOTIFYWEBCLIENT_H
#define SPOTIFYWEBCLIENT_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonObject>

class SpotifyWebClient : public QObject
{
    Q_OBJECT
public:
    explicit SpotifyWebClient(QObject *parent = nullptr, const QString &clientId = NULL, const QString &clientSecret = NULL);
    ~SpotifyWebClient();

    void setTokenInformation(QString accessToken, QString refreshToken, QString tokenType);

    QNetworkReply* sendGetRequest(QUrl);

signals:
    void accessTokenUpdated(QVariantMap);

public slots:
    void refreshAccessToken(QString refreshToken, QString callbackUrl);

private slots:
    void handleRefreshAccessTokenCompleted(QNetworkReply *);

private:
    QNetworkAccessManager *manager;
    QByteArray authenticationToken;

    QString refreshToken;
    QString accessToken;
    QString tokenType;


    void createAuthenticationToken(const QString &clientId, const QString &clientSecret);
    QByteArray encodeParams(const QUrlQuery &params);
};

// URLS
const QUrl API_REFRESH_TOKEN("https://accounts.spotify.com/api/token");
const QUrl API_ME("https://api.spotify.com/v1/me");
const QUrl API_ME_PLAYLISTS("https://api.spotify.com/v1/me/playlists");

// CONTENT TYPES
const char MIME_TYPE_XFORM [] = "application/x-www-form-urlencoded";
#endif // SPOTIFYWEBCLIENT_H
