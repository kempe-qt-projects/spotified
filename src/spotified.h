#ifndef SPOTIFIED_H
#define SPOTIFIED_H

#include <QObject>
#include <QTimer>
#include <QSettings>
#include "spotify_authentication_server.h"
#include "spotifywebclient.h"
#include "clientid.h"

class Spotified : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool authenticationRunning READ authenticationRunning NOTIFY authenticationRunningChanged)
    Q_PROPERTY(bool authenticated READ authenticated NOTIFY authenticatedChanged)
    Q_PROPERTY(QString appId READ appId CONSTANT)
    Q_PROPERTY(QString callbackUrl READ callbackUrl CONSTANT)

public:
    explicit Spotified(QObject *parent = nullptr);
    ~Spotified();

    SpotifyWebClient *spotifyClient() const { return webClient; }

    bool authenticationRunning() const { return m_authenticationRunning; }
    bool authenticated() const { return m_authenticated; }

    QString appId() const { return clientId; }
    QString callbackUrl() const { return clientCallbackUrl; }

signals:
    void authenticationRunningChanged();
    void authenticatedChanged();

public slots:
    void requestAccess();

private slots:
    void handleRequestAccessCompleted(QVariantMap);
    void handleRefreshAccessTokenCompleted(QVariantMap);

private:
    QSettings settings;
    SpotifyWebClient *webClient;

    bool m_authenticationRunning;
    bool m_authenticated;

    QString refreshToken;
    QString accessToken;
    QString tokenType;

    void attemptAuthentication();
};

#endif // SPOTIFIED_H
