#include "spotifyhomeplaylists.h"
#include <QDebug>

SpotifyHomePlaylists::SpotifyHomePlaylists(QObject *parent) : QAbstractListModel(parent) {
}

void SpotifyHomePlaylists::init() {
    QNetworkReply *reply = m_spotifyClient->sendGetRequest(API_ME_PLAYLISTS);

    connect(reply, &QNetworkReply::finished, this, [=]() {
        qDebug() << "FETCH PLAYLIST DONE";
        const auto jsonData = reply->readAll();
        const auto document = QJsonDocument::fromJson(jsonData);
        Q_ASSERT(document.isObject());
        qDebug() << "SpotifyHomePlaylists: " << document;

        const auto response = document.object().toVariantMap();

        for (QVariant item : response.value("items").toList()) {
            QVariantMap itemMap = item.toMap();
            QString name = itemMap.value("name").toString();
            QString image = itemMap.value("images").toList().at(0).toMap().value("url").toString();

            beginInsertRows(QModelIndex(), m_playlists.count(), m_playlists.count());
            m_playlists << SpotifyPlaylist(name, image);
            endInsertRows();
        }
        reply->deleteLater();
    });
}

int SpotifyHomePlaylists::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return m_playlists.count();
}

QVariant SpotifyHomePlaylists::data(const QModelIndex &index, int role) const {
    if (index.row() < 0 || index.row() >= m_playlists.count())
        return QVariant();

    const auto playlist = m_playlists[index.row()];

    switch (role) {
    case NameRole:
        return playlist.name();
    case ImageRole:
        return playlist.image();
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> SpotifyHomePlaylists::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[ImageRole] = "image";

    return roles;
}
