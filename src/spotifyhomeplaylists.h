#ifndef SPOTIFYHOMEPLAYLISTS_H
#define SPOTIFYHOMEPLAYLISTS_H

#include <QObject>
#include "spotifywebclient.h"
#include <QAbstractListModel>

class SpotifyPlaylist {
public:
    SpotifyPlaylist(const QString &name, const QString &image) {
        m_name = name;
        m_image = image;
    }

    QString name() const { return m_name; }
    QString image() const { return m_image; }

private:
    QString m_name;
    QString m_image;
};

class SpotifyHomePlaylists : public QAbstractListModel {
    Q_OBJECT
    Q_PROPERTY(SpotifyWebClient* spotifyClient READ spotifyClient WRITE setSpotifyClient NOTIFY spotifyClientChanged)

public:
    enum SpotifyPlaylistRoles {
        NameRole = Qt::UserRole + 1,
        ImageRole
    };

    explicit SpotifyHomePlaylists(QObject *parent = nullptr);

    SpotifyWebClient* spotifyClient() const { return m_spotifyClient; }
    void setSpotifyClient(SpotifyWebClient *client) {
        m_spotifyClient = client; emit spotifyClientChanged();
    }

    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;

protected:
    virtual QHash<int, QByteArray> roleNames() const override;

signals:
    void spotifyClientChanged();

public slots:
    void init();

private:
    SpotifyWebClient *m_spotifyClient;
    QList<SpotifyPlaylist> m_playlists;

};

#endif // SPOTIFYHOMEPLAYLISTS_H
