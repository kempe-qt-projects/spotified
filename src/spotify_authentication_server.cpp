#include "spotify_authentication_server.h"

SpotifyAuthenticationServer::SpotifyAuthenticationServer(QObject *parent) : QObject(parent) {
    tcpServer = new QTcpServer(this);
    connect(tcpServer, &QTcpServer::newConnection, this, &SpotifyAuthenticationServer::newConnection);

    if (!tcpServer->listen(QHostAddress::Any, 9999)) {
        qDebug() << "Server could not start";
    } else {
        qDebug() << "Server started!";
    }
}

SpotifyAuthenticationServer::~SpotifyAuthenticationServer() {
    if (tcpServer->isListening())
        tcpServer->close();
}

void SpotifyAuthenticationServer::newConnection() {
    socket = tcpServer->nextPendingConnection();
    connect(socket, &QTcpSocket::readyRead, this, &SpotifyAuthenticationServer::readyRead);
    connect(socket, &QTcpSocket::disconnected, socket, &QTcpSocket::deleteLater);
}

void SpotifyAuthenticationServer::readyRead() {
    QString requestContent = socket->readAll();

    QString requestPath = requestContent.split(QRegExp("[ \r\n][ \r\n]*"))[1];

    socket->write("HTTP/1.0 200 OK \r\n");
    socket->write("Content-Type: text/html; charset=\"utf-8\"\r\n");
    socket->write("Connection: close\r\n");

    if (requestPath.startsWith("/cb?")) {
        QString path = requestPath.split("?")[0];
        QString params = requestPath.split("?")[1];

        QVariantMap result;
        for (QString paramPair : params.split("&")){
            QStringList paramParts = paramPair.split("=");
            result.insert(paramParts[0], paramParts[1]);
        }

        qDebug() << result;
        emit requestAccessCompleted(result);
    }
    socket->disconnectFromHost();
}
