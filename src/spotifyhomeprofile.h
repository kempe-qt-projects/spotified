#ifndef SPOTIFYHOMEPROFILE_H
#define SPOTIFYHOMEPROFILE_H

#include <QNetworkReply>
#include <QDebug>
#include "spotifywebclient.h"

class SpotifyHomeProfile : public QObject
{
    Q_OBJECT
    Q_PROPERTY(SpotifyWebClient* spotifyClient READ spotifyClient WRITE setSpotifyClient NOTIFY spotifyClientChanged)

    Q_PROPERTY(QString displayName READ displayName NOTIFY displayNameChanged)
    Q_PROPERTY(QString profileUrl READ profileUrl NOTIFY profileUrlChanged)
    Q_PROPERTY(QString userId READ userId NOTIFY userIdChanged)
    Q_PROPERTY(QString profileImage READ profileImage NOTIFY profileImageChanged)
    Q_PROPERTY(QString userType READ userType NOTIFY userTypeChanged)
    Q_PROPERTY(QString userUri READ userUri NOTIFY userUriChanged)
public:
   explicit SpotifyHomeProfile(QObject *parent = nullptr);

    QString displayName() const { return m_displayName; }
    QString profileUrl() const { return m_profileUrl; }
    QString userId() const { return m_userId; }
    QString profileImage() const { return m_profileImage; }
    QString userType() const { return m_userType; }
    QString userUri() const { return m_userUri; }
    SpotifyWebClient* spotifyClient() const { return m_spotifyClient; }
    void setSpotifyClient(SpotifyWebClient *client) {
        m_spotifyClient = client;
        emit spotifyClientChanged();
    }

signals:
    void displayNameChanged();
    void profileUrlChanged();
    void userIdChanged();
    void profileImageChanged();
    void userTypeChanged();
    void userUriChanged();

    void spotifyClientChanged();

public slots:
    void init();

private:
    SpotifyWebClient *m_spotifyClient;

    QString m_displayName;
    QString m_profileUrl;
    QString m_userId;
    QString m_profileImage;
    QString m_userType;
    QString m_userUri;
};

#endif // SPOTIFYHOMEPROFILE_H
