#include "spotifyhomeprofile.h"


SpotifyHomeProfile::SpotifyHomeProfile(QObject *parent) : QObject(parent) {
}

void SpotifyHomeProfile::init() {
    QNetworkReply *reply = m_spotifyClient->sendGetRequest(API_ME);

    connect(reply, &QNetworkReply::finished, this, [=]() {
        const auto jsonData = reply->readAll();
        const auto document = QJsonDocument::fromJson(jsonData);
        Q_ASSERT(document.isObject());

        const auto response = document.object().toVariantMap();
        qDebug() << "SpotifyHomeProfile: " << response;


        m_displayName = response.value("display_name").toString();
        emit displayNameChanged();

        m_profileUrl = response.value("href").toString();
        emit profileUrlChanged();

        m_userId = response.value("id").toString();
        emit userIdChanged();

        m_profileImage = response.value("images").toList().at(0).toMap().value("url").toString();
        emit profileImageChanged();

        m_userType = response.value("type").toString();
        emit userTypeChanged();

        m_userUri = response.value("uri").toString();
        emit userUriChanged();

    });
    connect(reply, &QNetworkReply::finished, reply, &QNetworkReply::deleteLater);
}
