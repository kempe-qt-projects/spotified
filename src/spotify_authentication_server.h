#ifndef SPOTIFY_AUTHENTICATION_SERVER_H
#define SPOTIFY_AUTHENTICATION_SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

class SpotifyAuthenticationServer : public QObject
{
    Q_OBJECT
public:
    explicit SpotifyAuthenticationServer(QObject *parent = nullptr);
    ~SpotifyAuthenticationServer();

signals:
    void requestAccessCompleted(QVariantMap);

private slots:
    void newConnection();
    void readyRead();

private:
    QTcpServer *tcpServer;
    QTcpSocket *socket;
};

#endif // SPOTIFY_AUTHENTICATION_SERVER_H
