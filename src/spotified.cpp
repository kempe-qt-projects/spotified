#include "spotified.h"
#include <QGuiApplication>
#include <QDebug>

Spotified::Spotified(QObject *parent) : QObject(parent)
{
    webClient = new SpotifyWebClient(this, clientId, clientSecret);
    connect(webClient, &SpotifyWebClient::accessTokenUpdated, this, &Spotified::handleRefreshAccessTokenCompleted);

    refreshToken = settings.value("AppSettings/authenticationCode").toString();
    webClient->setTokenInformation(refreshToken, refreshToken, "");
    attemptAuthentication();
}

Spotified::~Spotified() {
    if (webClient != NULL) {
        qDebug() << "webClient->deleteLater()";
        webClient->deleteLater();
    }
}

void Spotified::requestAccess() {
    SpotifyAuthenticationServer *authServer = new SpotifyAuthenticationServer(qApp);
    connect(authServer, &SpotifyAuthenticationServer::requestAccessCompleted, this, &Spotified::handleRequestAccessCompleted);
    connect(authServer, &SpotifyAuthenticationServer::requestAccessCompleted, authServer, &SpotifyAuthenticationServer::deleteLater);
}

void Spotified::handleRequestAccessCompleted(QVariantMap result) {
    qDebug() << "New code recived: " << result;

    if (result.contains("code")) {
        refreshToken = result.value("code").toString();
        webClient->setTokenInformation("", "", "");
        attemptAuthentication();

    } else if (result.contains("error")) {
        // TODO handle and show error to user
        handleRefreshAccessTokenCompleted(QVariantMap());
        qDebug() << "RequestAccess error: " << result.value("error");

    } else {
        // Unknown error
        handleRefreshAccessTokenCompleted(QVariantMap());
        qDebug() << "RequestAccess: UNKNOWN ERROR";

    }
}


void Spotified::handleRefreshAccessTokenCompleted(QVariantMap result) {
    qDebug() << "handleRefreshAccessTokenCompleted: " << result;
    m_authenticationRunning = false;
    emit authenticationRunningChanged();

    if(result.contains("error") || result.isEmpty()) {
        // TODO handle and notify error
        accessToken = "";
        refreshToken = "";
        tokenType = "";

    } else {
        accessToken = result.value("access_token").toString();
        refreshToken = result.value("refresh_token", refreshToken).toString();
        tokenType = result.value("token_type").toString();

        qDebug() << "Saving token to settings: " << refreshToken;
        settings.setValue("AppSettings/authenticationCode", refreshToken);

        float expiresIn = result.value("expires_in", 3600).toFloat() * 0.75;
        qDebug() << "Renewing token in " << expiresIn << " sec";
        QTimer::singleShot(expiresIn * 1000, this, [=]() {
            webClient->refreshAccessToken(refreshToken, clientCallbackUrl);
        });

    }

    webClient->setTokenInformation(accessToken, refreshToken, tokenType);
    m_authenticated = !accessToken.isEmpty();
    emit authenticatedChanged();
}

void Spotified::attemptAuthentication() {
    qDebug() << "Requestion accesstoken with" << refreshToken;

    m_authenticationRunning = true;
    emit authenticationRunning();
    webClient->refreshAccessToken(refreshToken, clientCallbackUrl);
}
